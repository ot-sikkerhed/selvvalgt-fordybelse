# PLC projektet 

## Projektbeskrivelse
- Projektet handler om at undersøge om data kommunikation mellem 2 eller flere plc'ere er krypteret

![Eksmepel](../Billeder/PLC-projektet%20eksempel.png)

## Projektløsning
- Opsætning af to plc'ere igennem Siemens TIA-portal
    - TIA-portalen er et program udviklet af Siemens til at programmeringen af plc'ere
- Undersøglese af kommunikationen ved hjælp af WireShark
    - Er dataen krypteret?

## Refleksion
- Det tog en del tid at få adgang til to plc'ere, så vi valgte at fjerne en del af vores fokus fra dette projekt 
- Vi nåede dog at finde frem til en del information omkring protokoller som bliver brugt til kommunikationen mellem maskinerne 
- Vi nåede ikke at få færdiggjort projeket, da der kom for meget spildtid ved det, og vi valgte at sætte mere fokus på andre ting i projektet.