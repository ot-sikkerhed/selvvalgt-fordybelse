# Noter

## Netværks og kommunikations sikkerhed
- En af de store områder omkring ot-sikkerhed er netværks og kommunikations delen. Den typiske "best practies" har været at man bare har sagt, at så længe man holder sine maskiner væk fra internettet, og bare holder det til sit lokalnet, så vil man være sikker overfor trusler på netttet.
- Her kan vi også begynde at tænke sikkerheden ind på de tre nederste lag, især data-link laget, da vi arbeder med de lokale MAC-adresser 

## Links
- https://www.ds.dk/download?media=umb%3A%2F%2Fmedia%2Ff9f45428ec60458d83687da1d56a23b1 -> PDF omkring perspektiver om OT-Sikkerhed

- https://www.fortinet.com/content/dam/fortinet/assets/reports/report-state-ot-cybersecurity.pdf -> Rapport omkring ot sikkerhed fra fortinet

- https://via.ritzau.dk/pressemeddelelse/ny-rapport-peger-pa-stort-skifte-for-ot-sikkerheden?publisherId=13560982&releaseId=13690283 -> Artikel omkring rapporten 

- https://pctechmag.com/2023/02/6-common-vulnerabilities-in-ot-environments-and-how-to-address-them/ -> Link til 6 svagheder

- https://ieeexplore.ieee.org/search/searchresult.jsp?newsearch=true&queryText=ot%20security - IEEE (forskning omkring ot-sikkerhed)

- https://sektorcert.dk/publikationer/ - Information omkring forskellige ting

- https://securiot.dk/iec62443/ - overblik over IEC-62443