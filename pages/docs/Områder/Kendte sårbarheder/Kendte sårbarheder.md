# Kendte sårbarheder
[10 kendte sårbarheder](https://www.checkpoint.com/downloads/products/top-10-cybersecurity-vulnerabilities-threat-for-critical-infrastructure-scada-ics.pdf)

## Legacy Software
- Legacy software er når vi kører med gammelt software på systemer som ikke er blevet opdateret igennem længere tid, derfor vil de sikkerhedshuller som er blevet fundet gennem tiden ikke være blevet opdateret og der vil derfor være mulighed for at blive kompromitteret på den måde. I OT-sikkerhed kører legacy systemer oftest offline og derved mindsker der chancer for at blive angrebet online. Dermed ikke sagt det ikke er umuligt, der er før set at folk har haft maskiner af ældre datoer som netop kører på legacy software og derved bliver kompromitteret. 

## Default Configuration
- Man har ikke lavet en ændre af ddefault configuration på systemerne, hvilket gør at trussels aktører let kan få adgang til systemerne ved brug af default login som eksempelvis Admin:Admin

## Lack of Encryption
- Dataen er ikke krypteret, hvilket gør at trussels aktører kan anskaffe sig daten. 

## Remote Access Policies
- Skulle det blive nødvendigt at opdatere noget software remote, bliver det nødvendigt med et authenticator system som gør det muligt at connect til serveren. Her er det meget vigtig, vi ikke bruger default-configuration, da risikoen for at blive kompromitteret vil stige markant ved at bruge “standardiseret” passwords, som kan findes i forskellige lister med passwords som fx. Rockyou

## Policies and Procedures
- Skulle det ske, at man bliver kompromitteret, er det vigtig at have en plan for hvordan man skal agere i sådan en situation. Det hjælper blandt andet med at sikre alle ved, hvad der skal gøres, og man ikke “glemmer” noget i en så stresset situation.

## Lack of Network Segmentation 
- Der er en mangel på segmentering af netværket, hvilket gør at trussels aktører kan hoppe rundt på netværket, istedet for at være indkapslet til bestemte dele af det.

## DDoS Attacks
- Når man vælger at tilgå en maskine uden at være on premise, vil der også være en offentlig IP adresse, hvilket gør det muligt at sende “pakker” til maskinen og overbelaste den. Det kan fx medføre at maskinen ikke kan snakke sammen med andre maskiner og derved kan produktionen ikke fortsætte som medfører tab af omsætning.  

## Escalation of privaleges 
- Når vi snakker escalation of privaleges er der 2 forskellige måder dette kan udføre på.
Først har vi horizontal escalation of privileges. Når vi snakker den horisontale måde, går det i alt enkelthed ud på at få adgang til så mange kontorer som muligt, da man for hver konto har en større angrebsflade.

Så har vi vertikal her forsøger truselsakøtrene at få adgang en brugere som har flere rettigheder end den han allerede har kompromitteret og derved få flere og flere rettigheder i systemet og tilsidst side med fuld adgang til systemet. 
