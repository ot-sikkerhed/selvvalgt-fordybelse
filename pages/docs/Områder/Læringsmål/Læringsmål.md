# Beskrivelse af læringsmål samt hvordan vi kommer omkring dem

### Det hele er i OT perpesktiv
## Viden

Den studerende har viden om

- Kenskab til ofte anvendte angrabsmetoder 
- It-sikkerhedsprincipper til design af sikre systemer 
- It-sikkerhedstiltag og kan reflektere over forretningsbehov i forhold hertil. 


## Færdigheder

Den studerende kan 

- Sætte forsvars metoder op mod de mest anvendte angrabs metoder
- Anvende, vurdere og formidle it-sikkerhedsstandarder ift. forretningsbehov
-  Mestre relevante designprincipper i forbindelse med udvikling af sikre systemer.

## Kompetencer

Den studerende kan 

- Håndtere udviklingsorienterede situationer herunder:
    - Sikre it-systemer vha. relevante krypteringstiltag 
- Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-sikkerhed/ot-sikkerhed 


# Løsning af læringsmål
## Viden
### Kenskab til ofte anvendte angrabsmetoder 
- De mest kendte angrebsmetoder kommer fra angreb på kendte sårbarheder på det legacy software som ofte kører på ot-maskinerne.

### It-sikkerhedsprincipper til design af sikre systemer 
- En af principperne man kan følge er ved at følge profinet, som er med til at sikre at det er begrænset hvad for noget data som bliver sendt til maskinerne over det interne netværk. 
- Et andet princip er det som er standarden i den virkelige verden, hvor man sikre sig at maskinerne ikke kan tilgå internettet.

### It-sikkerhedstiltag og kan reflektere over forretningsbehov i forhold hertil. 
- Netværk og kommunikations sikkerhed

## Færdigheder
### Sætte forsvars metoder op mod de mest anvendte angrabs metoder
- Ofter opdateringer/opgradering af legacy software, så de lever op til nyere sikkerhedskrav
- Opsætning af firewall/genrelt opsætning af en stærk infrastruktur
- IDS/IPS (monitorering af netværket)  

### Anvende, vurdere og formidle it-sikkerhedsstandarder ift. forretningsbehov
- Se efter om det lever op til de forskellige standarder (DS-62443, ISO 27000 serien)
- Se efter om det gør brug af de forskellige protokoller (Profinet)

###  Mestre relevante designprincipper i forbindelse med udvikling af sikre systemer.
- Opsætning af systmer med en stærk infrastruktur, og et stærkt regelsæt
- Opsætning af programmer som gør brug af Profinet

## Kompetancer
### håndter risikovurdering af programkode for sårbarheder.
- Lave en risikovurdering af eventuelle bagdøre
- Sikre at programmet lever og til standarderne/best practies

### Håndtere udviklingsorienterede situationer herunder:
- Sikre it-systemer vha. relevante krypteringstiltag 
    - Som standard bliver trafikken krypteret via tls når den bliver sendt rundt på netværket

### Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-sikkerhed 
- Sikre at programmer, maskiner og infrastrukturen lever op til de forskellige standarder og best pratcies til de givenden områder