# Profinet

## Hvad er det?
- Profinet er en kommunikationsprotokol som er baseret på internationale standarder, den bruges til at komponenter kan kommunikerer med hinanden og køre automatisk. 
Profinet er baseret på ethernet, hvilket vil sige vi bruger switches routere og kabler til at skabe en lokal forbindelse så der ikke er snak ud til omverdenen.

## Hvordan virker det?
- Profinet selv liller på lag 7 i OSI modellen altså applications niveau herfra sender den så beskeder direkte til lag 2 (data link laget) derved kan vi garanterer vi ikke kommer i “kø” når vi skal sende pakker rundt i det interne system, og kan dermed sikre os at vores maskiner får de hurtigst mulige svartider.