# Modbus

[Modbus video](https://www.youtube.com/watch?v=txi2p5_OjKU&ab_channel=RealPars)

https://www.sealevel.com/2021/02/12/securing-modbus-communications-modbus-tls-and-beyond/


## Hvad er det 
- Modbus også kendt som Profibus, er ligesom Profinet en kommunikationsprotokol som blev udviklet i 1979 for at gøre det muligt at automatisere kommunikation mellem to maskiner. 
Modbus virker ud fra “master slave” relationen. I et master slave forhold foregår kommunikationen altid i par, den ene enhed skal sende en besked af sted og derefter vente på svar, masteren styre herfra alle opgaver som skal udføres. Masteren kan være en Plc controller som sender besked ud til en sensor (slaven) 
