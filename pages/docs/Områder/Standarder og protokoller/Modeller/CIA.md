# CIA-Modellen

![CIA](../../../Billeder/CIA.png)

## Hvad viser modellen 
- CIA-modellen indeholder, Confidentiality, Integrity og Availability, og den bruges til at sikkerheden bliver overhold 
- Confidentiality, fortæller om tingene er fortroligt, så andre uvedkommen ikke kan se hvad er stå i dataen.
- Integrity, fortæller om vi kan stole på tingene som står i dataen.
- Availability, fortæller om tingene er tilgængelig når vi skal bruge dem 

## Hvordan kan den bruges i forhold til OT-sikkerhed
- Når man bruger CIA-modellen i forhold til OT-sikkerhed, er det største fokus på tilgængeligheden, for hvis tilgængligheden forsvinder, så kan det koste virksomheden mange penge
- På den måde kan man sige CIA-modellen er lidt vendt om i forhold til hvor fokuset er i IT-sikkerhed. 