# Purdue 

![Purdue](../../../Billeder/Purdue%20model.png)

## Hvad viser modellen

- Modellen viser hvor man typisk vil lave grænsen mellem IT-sikkerhed og OT-sikkerhed.
- Hvis man tager udgangspunkt i OSI-modellen, så kan man ud fra modellen se at OT-sikkerhed "stopper" efter lag 3, da de første lag er det som forgår inden på det lokale netværk. 
- Modellen viser også hvad det er for nogle bestemte område der ligger inden for den "Industrielle sikkerheds zone", og hvad for nogle områder som ligger uden for den zone.
- Man kan også ud fra modellen se en af sikkerhedsforanstaltningerne som typisker bliver brugt, en firewall til at sikre sit netværk.
- De forskellige niveauer er:
    - Level 0 - Field Devices, indholder sensorer og aktuatorer til celle-, linje-, proces- eller DCS-løsningen.
    - Level 1 - Local Controllers, systemer og enheder som kontrollere de forskellige ting som ligger i level 0.
    - Level 2 - Local Supervisory, som er alle monitoreringsværktøjer til de enkelte systemer og enheder 
    - Level 3 - Site-wide Supervisory, som er monitoreringsværktøjer og support til hele arbejdspladsen 
    - Level 4 - Buisness Networks, alt netværket på de lokale arbejdspladser 
    - Level 5 - Enterprise Networks, Netværket for hele enterprisen

## Hvad bruger man modellen til
- Modellen bruges til at skabe et overblik over hvilke lag som er relevante for OT-sikkerheden, og hvad for nogle elementer som ligger udenfor. 
- Modellen er med til at skabe et grundlag for IEC-62443, som er en IEC standard der bliver brugt til Informationssikkerhed for virksomheder som arbejder med OT.
- Jo lavere niveau man er på i modellen, jo mere adgang har man til de kritikale processer, som typisk i OT-sikkerhed ikke har mange sikkerhedsforanstaltninger.  

https://www.sans.org/blog/introduction-to-ics-security/ 

https://www.sans.org/blog/introduction-to-ics-security-part-2/

https://www.sans.org/blog/introduction-to-ics-security-part-3/


