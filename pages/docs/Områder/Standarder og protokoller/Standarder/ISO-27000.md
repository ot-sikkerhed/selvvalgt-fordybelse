# ISO-27000 serien

## Hvad er ISO-27000 serien 
- ISO-27000 serien er en interation standard, der bliver brugt til etableringen af et ledelsessystem for informationsikkerhed 
- Serien tager udgangspunkt i en risikobaseret tilgang til styringen af informationsikkerhed (den kigger på tingene ud fra risikoerne)

### ISO-27001
- ISO-27001 er en normativ standard, hvilket betyder at den stiller krav til hvordan de forskellige informationssikkerhedsledelsessystemer skal implementeres og vedligeholdes af virksomhederne.
- ISO-27001 er god, fordi den tager udgangspunkt i den enkelte virksomheds risikoprofil, og på den måde giver et udgangspunkt for valget af sikkerhedsforanstaltninger for den enkelte virksomhedsbehov
- Det er også en international standard, så det viser omverden at hvad virksomheden lever op til at informationssikkerhedskrav

### ISO-27002
- ISO-27002 indholder de forskellige retningslinjer, her i blandt en liste af anderkendte kontroller, der kan bruges som en hjælp til virksomhedens implementeringer og udvælgelser af de kontroller der er nødvendt og giver mest mening, for at opnå et passende informationssikkerhedsniveau.