# OT-Sikkerhed

# Indhold

Projektet hander om efterforskningen af OT-sikkerhed, hvor der blandt andet bliver kigger på standarder og protokoller, som man vil kunne følge ude i det virkelige liv.

# Læringsmål ordnes efter viden, færdigheder og kompetencer

Den studerende opsætter selv læringsmål (skabelon herunder) ud fra det valgte emne og indenfor rammerne i uddannelsens mål for læringsudbytte fra den nationale studieordning kapitel 1, se: [https://esdhweb.ucl.dk/D22-1980440.pdf](https://esdhweb.ucl.dk/D22-1980440.pdf)  
Det er ikke alle læringsmål der skal opfyldes, vælg dem der passer til det valgte fordybelsesområde.

Læringsmål skal ordnes efter viden, færdigheder og kompetencer

### Det hele er i OT perpesktiv
## Viden

Den studerende har viden om

- Kenskab til ofte anvendte angrebsmetoder 
- It-sikkerhedsprincipper til design af sikre systemer 
- It-sikkerhedstiltag og kan reflektere over forretningsbehov i forhold hertil. 


## Færdigheder

Den studerende kan 

- Sætte forsvars metoder op mod de mest anvendte angrebsmetoder
- Anvende, vurdere og formidle it-sikkerhedsstandarder ift. forretningsbehov
- Mestre relevante designprincipper i forbindelse med udvikling af sikre systemer.

## Kompetencer

Den studerende kan 

- Håndtere udviklingsorienterede situationer herunder:
    - Sikre it-systemer vha. relevante krypteringstiltag 
- Selvstændigt tilegne sig viden, færdigheder og kompetencer indenfor it-sikkerhed/ot-sikkerhed   

### Standarder 
IEC-62443

ISO-27001

ISO-27002


# Milepæls plan

Her angives deadlines (datoer) for hvilke milepæle der arbejdes efter i projektet
Der skal angives en milepæl pr. uge fra og med uge 36 til og med uge 46

| Deadline   | Milepæl                              |
| :--------- | :------------------------------------|
| 2023-09-05 | Projekt beskrevet og planlagt        |
| 2023-09-12 | Undersøg arbejdsområder              |
| 2023-09-19 | Helhedsbilled af området             |
| 2023-09-26 | Udforsk de mest kendte sårbarheder     |
| 2023-10-03 | Specering af et specifikt område                                |
| 2023-10-10 | Opgaver på tryhackme                                          |
| 2023-10-24 | Mini projekt opstart                                     |
| 2023-10-31 | Mini projekt fortsættelse                                      |
| 2023-11-07 | Mini projekt færdiggøres                                     |
| 2023-11-14 | Eksamensaflevering færdig            |

# Link til gitlab projekt 

https://ot-sikkerhed.gitlab.io/selvvalgt-fordybelse/ 

# Andet

Hvis der er andet du mener er vigtigt at have med i denne plan angives der her eller i punkter du selv tilføjer
